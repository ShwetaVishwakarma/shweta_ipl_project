let csvToJson = require('convert-csv-to-json');
const path = require("path");
let matchesInputName = path.join(__dirname, "./data/matches.csv");
let deliveriesInputName = path.join(__dirname, "./data/deliveries.csv");

let matchesOutputName = path.join(__dirname, "./data/matches.json");
let deliveriesOutputName = path.join(__dirname, "./data/deliveries.json");

csvToJson.fieldDelimiter(',').generateJsonFileFromCsv(matchesInputName, matchesOutputName);
csvToJson.fieldDelimiter(',').generateJsonFileFromCsv(deliveriesInputName, deliveriesOutputName);

