const path = require('path')
const fs = require('fs')

const { matchesPerYear, matchesWonPerTeam, extraRunConcededPerTeam, topTenEconomicalBowlers } = require("./ipl.js")

const deliveries = require(path.join(__dirname, "../data/deliveries.json"));
const matches = require(path.join(__dirname, "../data/matches.json"));

const executeIplFunction = (
    deliveries,
    matches,
    outputFile,
    iplCb
) => {
    const iplOutput = iplCb(matches, deliveries);
    console.log(iplOutput);
    const outputFilePath = path.join(__dirname, `../public/output/${outputFile}.json`);
    fs.writeFile(outputFilePath, JSON.stringify(iplOutput), (err, data) => {
        if (err) throw err;
        console.log("complete");
    })
}

executeIplFunction(deliveries, matches, "matchesPerYear",matchesPerYear)
executeIplFunction(deliveries, matches, "matchesWonPerTeam",matchesWonPerTeam)
executeIplFunction(deliveries, matches, "extraRunConcededPerTeam",extraRunConcededPerTeam)
executeIplFunction(deliveries, matches, "topTenEconomicalBowlers",topTenEconomicalBowlers)