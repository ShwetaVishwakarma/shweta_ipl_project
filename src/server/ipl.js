//Problem 1
module.exports.matchesPerYear = (matchesData) => {
  // let matchCountPerYear = {};
  //     matchesData.forEach(element => {
  //     if (matchCountPerYear.hasOwnProperty(element.season)) {
  //         matchCountPerYear[element.season] += 1;
  //     }
  //     else {
  //         matchCountPerYear[element.season] = 1;
  //     }
  // });
  // //console.log(matchCountPerYear);
  // return matchCountPerYear;
  const matchCountPerYear = matchesData.reduce(
    (matchCountPerYear, elementMatchData) => {
      if (matchCountPerYear[elementMatchData.season]) {
        matchCountPerYear[elementMatchData.season] += 1;
      } else {
        matchCountPerYear[elementMatchData.season] = 1;
      }
      return matchCountPerYear;
    },
    {}
  );
  return matchCountPerYear;
};

//Problem 2
module.exports.matchesWonPerTeam = (matchesData) => {
  let outputObj = {};
  matchesData.forEach((matchData) => {
    if (outputObj.hasOwnProperty(matchData.winner)) {
      if (outputObj[matchData.winner].hasOwnProperty(matchData.season)) {
        outputObj[matchData.winner][matchData.season] += 1;
      } else {
        outputObj[matchData.winner][matchData.season] = 1;
      }
    } else {
      outputObj[matchData.winner] = {};
    }
  });
  //console.log(outputObj);
  return outputObj;
};

//Problem 3
module.exports.extraRunConcededPerTeam = (matchesData, deliveriesData) => {
  //let extraRun = {};
  let matches2016 = matchesData.filter((matchData) => {
    if (matchData.season === "2016") {
      return true;
    }
  });
  // deliveriesData.forEach((deliveryData) => {
  //     if (matches2016.hasOwnProperty(deliveryData.match_id) && deliveryData.extra_runs!=='0') {
  //         if (extraRun.hasOwnProperty(deliveryData.bowling_team)) {
  //             extraRun[deliveryData.bowling_team] += +deliveryData.extra_runs;
  //             console.log(deliveryData.bowling_team+" "+ extraRun[deliveryData.bowling_team])
  //         } else {
  //             extraRun[deliveryData.bowling_team] = +deliveryData.extra_runs;
  //             console.log(deliveryData.bowling_team+" "+ extraRun[deliveryData.bowling_team])
  //         }
  //     }
  // })
  // return extraRun;
  const extraRunConceded = deliveriesData.reduce((extraRun, deliveryData) => {
    if (
      matches2016.hasOwnProperty(deliveryData.match_id) &&
      deliveryData.extra_runs !== "0"
    ) {
      if (extraRun.hasOwnProperty(deliveryData.bowling_team)) {
        extraRun[deliveryData.bowling_team] += +deliveryData.extra_runs;
      } else {
        extraRun[deliveryData.bowling_team] = +deliveryData.extra_runs;
      }
    }
    return extraRun;
  }, {});
  return extraRunConceded;
};

//Problem 4
module.exports.topTenEconomicalBowlers = (matchesData, deliveriesData) => {
  let matchesIn2015 = matchesData.filter((matchData) => {
    if (matchData.season === "2015") {
      return true;
    }
  });
  let deliveriesIn2015 = deliveriesData.filter((deliveryelement) => {
    if (
        matchesIn2015.find((matchElement) => {
        if (matchElement.id === deliveryelement.match_id) {
          return true;
        }
      })
    ) {
      return deliveryelement;
    }
  });
  // let bowlers=[];
  // deliveriesIn2015.forEach((element)=>{
  //     if(bowlers.includes(element.bowler)){

  //     }else{
  //         bowlers.push(element.bowler)
  //     }
  // })
  let bowlers = deliveriesIn2015.reduce((bowlers, deleiveryIn2015) => {
    if (!bowlers.includes(deleiveryIn2015.bowler)) {
      bowlers.push(deleiveryIn2015.bowler);
    }
    return bowlers;
  }, []);
  //console.log(bowlers)
  function calculate_economy(player, deliveriesData2015) {
    const playerDelieveryData = deliveriesData2015.filter((Deliveryelement) => {
      if (Deliveryelement.bowler === player) {
        return true;
      }
    });
    let bowls = 0;
    const runs = playerDelieveryData.reduce(
      (calculateRuns, playerDelieveriesData) => {
        calculateRuns += +playerDelieveriesData.total_runs;
        bowls++;
        return calculateRuns;
      },
      0
    );

    return runs / (bowls / 6);
  }
  let economyOfBowlers = [];
  bowlers.forEach((bowler) => {
    economyOfBowlers.push([
      bowler,
      calculate_economy(bowler, deliveriesIn2015),
    ]);
  });
  economyOfBowlers.sort(function (a, b) {
    return a[1] - b[1];
  });
  //console.log(economyOfBowlers);
  const topTenEconomicalBowlers = economyOfBowlers.slice(0, 10);
  return topTenEconomicalBowlers;
};
